package com.mikro.phonebook.repositories;

import com.mikro.phonebook.models.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<Contact,Long> {


    Contact findByPhoneNumber(String phoneNumber);
}
