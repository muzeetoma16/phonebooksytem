package com.mikro.phonebook.service.impl;

import com.mikro.phonebook.dto.ContactRegisterRequest;
import com.mikro.phonebook.dto.ContactUpdateRequest;
import com.mikro.phonebook.dto.DtoResponse;
import com.mikro.phonebook.models.Contact;
import com.mikro.phonebook.repositories.ContactRepository;
import com.mikro.phonebook.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ContactServiceImpl implements ContactService {
    private final ContactRepository contactRepository;

    @Override
    public ResponseEntity<DtoResponse<Contact>> register(ContactRegisterRequest contactRegisterRequest) {

        Contact _contact = contactRepository.findByPhoneNumber(contactRegisterRequest.getPhoneNumber());

        if(Objects.nonNull(_contact)){
            return new ResponseEntity<>(new DtoResponse<>(null,"Contact already exists",HttpStatus.CONFLICT.value()), HttpStatus.CONFLICT);
        }

        Contact contact = Contact.builder()
                .address(contactRegisterRequest.getAddress())
                .name(contactRegisterRequest.getName())
                .phoneNumber(contactRegisterRequest.getPhoneNumber())
                .build();

        contactRepository.save(contact);

        return new ResponseEntity<>(new DtoResponse<>(contact,"Created",HttpStatus.CREATED.value()), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DtoResponse<Contact>> update(ContactUpdateRequest contactUpdateRequest) {

        Optional<Contact> contact = contactRepository.findById(contactUpdateRequest.getId());

        if(contact.isEmpty()){
            return new ResponseEntity<>(new DtoResponse<>(null,"Contact does not exist",
                    HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }

        Contact __contact = contactRepository.findByPhoneNumber(contactUpdateRequest.getPhoneNumber());

        //check if the phone number to be updated is existent already
        if(Objects.nonNull(__contact) &&
                !contactUpdateRequest.getPhoneNumber().equals(contact.get().getPhoneNumber())){
            return new ResponseEntity<>(new DtoResponse<>(null,"Phone number belongs to a different contact. Please change!",
                    HttpStatus.CONFLICT.value()), HttpStatus.CONFLICT);
        }

        Contact contactToEdit = contact.get();

        contactToEdit.setAddress(contactUpdateRequest.getAddress());
        contactToEdit.setName(contactUpdateRequest.getName());
        contactToEdit.setPhoneNumber(contactUpdateRequest.getPhoneNumber());
        Contact editedContact = contactRepository.save(contactToEdit);

        return new ResponseEntity<>(new DtoResponse<>(editedContact,"Updated",HttpStatus.ACCEPTED.value()), HttpStatus.ACCEPTED);

    }

    @Override
    public ResponseEntity<DtoResponse<Contact>> getById(Long id) {
        if(id==0 || id < 1){
            return new ResponseEntity<>(new DtoResponse<>(null,"Invalid Id passed",
                    HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }

        Optional<Contact> contactOptional = contactRepository.findById(id);

        if(contactOptional.isEmpty()){
            return new ResponseEntity<>(new DtoResponse<>(null,"Contact does not exist",
                    HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new DtoResponse<>(contactOptional.get(),"Contact found!",HttpStatus.OK.value()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DtoResponse<Void>> deleteById(Long id) {
        if(id==0 || id < 1){
            return new ResponseEntity<>(new DtoResponse<>(null,"Invalid Id passed",
                    HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        }

        Optional<Contact> contactOptional = contactRepository.findById(id);
        if(contactOptional.isEmpty()){
            return new ResponseEntity<>(new DtoResponse<>(null,"Contact not found!",
                    HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
        contactRepository.delete(contactOptional.get());

        return new ResponseEntity<>(new DtoResponse<>(null,"Contact deleted!",
                HttpStatus.OK.value()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DtoResponse<Page<Contact>>> getAll(int page, int record) {

        Pageable pageable = PageRequest.of(page,record);

        Page<Contact> contactPage = contactRepository.findAll(pageable);

        return new ResponseEntity<>(new DtoResponse<>(contactPage,"",
                HttpStatus.OK.value()), HttpStatus.OK);
    }
}
