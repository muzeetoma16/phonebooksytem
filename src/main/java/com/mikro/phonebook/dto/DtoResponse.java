package com.mikro.phonebook.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Data
@Builder
public class DtoResponse<T> {

    T data;
    String message;
    int status;
}
