package com.mikro.phonebook.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ContactRegisterRequest {

    @NotBlank(message = "Contact Name should not be blank")
    private String name;

    @NotBlank(message = "PhoneNumber should not be blank")
    private String phoneNumber;

    @NotBlank(message = "Address should not be blank")
    private String address;


}
