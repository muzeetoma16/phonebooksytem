package com.mikro.phonebook.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ContactUpdateRequest {

    @NotNull(message = "Id cannot be null")
    private Long id;

    @NotBlank(message = "Contact Name should not be blank")
    private String name;

    @NotBlank(message = "PhoneNumber should not be blank")
    private String phoneNumber;

    @NotBlank(message = "Address should not be blank")
    private String address;

}
