package com.mikro.phonebook.controllers;

import com.mikro.phonebook.dto.ContactRegisterRequest;
import com.mikro.phonebook.dto.ContactUpdateRequest;
import com.mikro.phonebook.dto.DtoResponse;
import com.mikro.phonebook.models.Contact;
import com.mikro.phonebook.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class ContactController {

    private final ContactService contactService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DtoResponse<Contact>> register(@Valid @RequestBody ContactRegisterRequest contactRegisterRequest){
        return contactService.register(contactRegisterRequest);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DtoResponse<Contact>> update(@Valid @RequestBody ContactUpdateRequest contactUpdateRequest){
        return contactService.update(contactUpdateRequest);
    }

    @GetMapping(path = "/get-by-id", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DtoResponse<Contact>> getById(@RequestParam(defaultValue = "1", name = "id") Long id){
        return contactService.getById(id);
    }

    @GetMapping(path = "/get-all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DtoResponse<Page<Contact>>> getAll(@RequestParam(defaultValue = "0", name = "page") int page,
                                         @RequestParam(defaultValue = "20", name = "record") int record){
        return contactService.getAll(page, record);
    }

    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DtoResponse<Void>> deleteById(@RequestParam(name = "id") Long id){
        return contactService.deleteById(id);
    }


}
