package com.mikro.phonebook.models;


import javax.persistence.*;
import lombok.*;


@Entity
@Table(name="address")
@Data
@AllArgsConstructor
@Setter
@NoArgsConstructor
@Builder
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "phone_number", unique = true)
    private String phoneNumber;


}
