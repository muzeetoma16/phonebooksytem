package com.mikro.phonebook.service;

import com.mikro.phonebook.dto.ContactRegisterRequest;
import com.mikro.phonebook.dto.ContactUpdateRequest;
import com.mikro.phonebook.dto.DtoResponse;
import com.mikro.phonebook.models.Contact;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface ContactService {

    ResponseEntity<DtoResponse<Contact>> register(ContactRegisterRequest doctorRegisterRequest);

    ResponseEntity<DtoResponse<Contact>> update(ContactUpdateRequest doctorUpdateRequest);

    ResponseEntity<DtoResponse<Contact>> getById(Long id);

    ResponseEntity<DtoResponse<Void>> deleteById(Long id);

    ResponseEntity<DtoResponse<Page<Contact>>> getAll(int page, int record);
}
